#include<iostream>
#include<time.h>
#include<unistd.h>
using namespace std;

#define FINISH_LINE 50
#define MAX_STAMINA 100
#define STAMINA_CRASH (FINISH_LINE/3)
#define NO_OF_RACER 5

class Racer;

class Jockey {
    public:
    
    int slipperySlope() {
        return rand() % 2;
    }
};

class Horse {
    int position;
    int stamina;
    

    public:
    Horse() {
        position = 1;
        stamina = MAX_STAMINA;
    }
    
    void setPosition(int steps);
    int getPosition();
    void setStamina(int stamina);
    int getStamina(void);
};

void Horse::setPosition(int steps) {
    position += steps;
}

int Horse::getPosition() {
    return position;
}

void Horse::setStamina(int stamina) {
    this->stamina = stamina;
}
    
int Horse::getStamina(void) {
    return stamina;
}

class Racer {
    Horse horse;
    Jockey jockey;
    int id;
    
    public:
    void setPosition();
    int getPosition();
    void printPosition();
};

void Racer::setPosition() {
    
    int dice_roll = rand() % 6 + 1;
    int stamina = horse.getStamina();
 
    if(stamina > 0)
    {
        int distance;
        if(!jockey.slipperySlope()) {
            distance = dice_roll*stamina/100;
       
        }
        else {
            distance = (dice_roll*stamina/100)/2;
            
        }
        horse.setPosition(distance);
        stamina = stamina-distance;
        horse.setStamina(stamina);
        
    }
}

int Racer::getPosition() {
    return horse.getPosition();
}
    
void Racer::printPosition() {
    int position = horse.getPosition();
    for(int i=1; i <= FINISH_LINE || i<= position; i++) {
        if(i == position)
            cout<<"H";
        else 
            cout<<"-";  
        }
        cout<<endl;
}

class Race {
  Racer racer[NO_OF_RACER];
  int winner;
  
  public:
  
  Race() {
      srand(time(NULL));
      winner = -1;
  }
  
  void startRace();
  int getWinner() {
      return winner;
  }
  
};

void Race::startRace() {
    cout << string(100, '\n' );
           
    for(int i=0; i<5; i++) {
        racer[i].printPosition();
    }
    bool finish = false;
    while(!finish) {
            
        for(int i=0; i<NO_OF_RACER; i++) {
            racer[i].setPosition();
            if(racer[i].getPosition() >= FINISH_LINE) {
                winner = i+1;
                finish = true;
                break;
            }
              
        }
          
        sleep(1);
        cout << string(100, '\n' );
           
        for(int i=0; i<5; i++) {
            racer[i].printPosition();
        }
    }
}

int main() {
    Race r;
    r.startRace();
    cout<<"HORSE "<<r.getWinner()<<" WON THE RACE"<<endl;
}

